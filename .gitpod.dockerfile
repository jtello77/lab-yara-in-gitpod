FROM buildpack-deps:focal

RUN uname -a
RUN apt-get update
RUN apt-get install yara -y
RUN apt install python3-pip -y
RUN pip3 install -U oletools
RUN apt-get install -y whois
RUN apt-get install -y iputils-ping